import './UsableContentItem.dart';

class UsableList {
  getList() {
    return [
      new UsableContentItem(
        headerImagePath: 'example11.jpg',
        teaser: 'Trump applaudiert sich selbst zu',
        detail: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.'
      ),
      new UsableContentItem(
        headerImagePath: 'example12.jpg',
        teaser: 'Proteste gegen den Klimawandel',
        detail: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.'
      ),
      new UsableContentItem(
        headerImagePath: 'example13.jpg',
        teaser: 'Weitblick bei der Anlagestrategie',
        detail: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.'
      ),
      new UsableContentItem(
        headerImagePath: 'example14.jpg',
        teaser: 'Im Winter zelten – geht das?',
        detail: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.'
      ),
      new UsableContentItem(
        headerImagePath: 'example15.jpg',
        teaser: 'Remis zwischen Schalke und dem Sc Freiburg',
        detail: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.'
      ),
    ];
    /*
    return [
      new UsableContentItem(
        headerImagePath: 'example21.jpg',
        teaser: 'Globe',
        detail: 'Source: www.unsplash.com \nImage: Gabrielle Rocha-Rios \nDate: 01.01.2001'
      ),
      new UsableContentItem(
        headerImagePath: 'example22.jpg',
        teaser: 'Rock',
        detail: 'Source: www.unsplash.com \nImage: Matt Cannon \nDate: 01.01.2001'
      ),
      new UsableContentItem(
        headerImagePath: 'example23.jpg',
        teaser: 'Signage',
        detail: 'Source: www.unsplash.com \nImage: Francois Dallay \nDate: 23.08.2019'
      ),
      new UsableContentItem(
        headerImagePath: 'example24.jpg',
        teaser: 'Grey Spiral',
        detail: 'Source: www.unsplash.com \nImage: Sasha Yudaev \nDate: 10.11.2019'
      ),
      new UsableContentItem(
        headerImagePath: 'example25.jpg',
        teaser: 'Giraffe',
        detail: 'Source: www.unsplash.com \nImage: Andy Holmes \nDate: 27.11.2019'
      ),
    ];
    
     */
  }
  
  getActionDescription() {
    return 'Like';
  }
}