import 'dart:ui' as ui;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './UsableContentItem.dart';

class DetailView extends StatelessWidget {
  DetailView({Key key, this.item, this.actionDescription}) : super(key: key);
  
  final UsableContentItem item;
  final String actionDescription;
  
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        backgroundColor: Color.fromRGBO(1, 17, 38, 0.7),
        leading: MediaQuery(
          data: MediaQueryData(textScaleFactor: MediaQuery.textScaleFactorOf(context)),
          child: CupertinoButton(
            onPressed: () => Navigator.pop(context),
            padding: EdgeInsets.all(0),
            child: Text('Close')
          )
        ),
        middle: MediaQuery(
          data: MediaQueryData(textScaleFactor: MediaQuery.textScaleFactorOf(context)),
          child: Text(
            item.teaser,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: Color.fromRGBO(242, 235, 236, 1),
              fontWeight: FontWeight.normal
            )
          )
        ),
        trailing: MediaQuery(
          data: MediaQueryData(textScaleFactor: MediaQuery.textScaleFactorOf(context)),
          child: CupertinoButton(
            onPressed: () => item.doPreferredAction(context),
            padding: EdgeInsets.all(0),
            child: Text(actionDescription)
          )
        ),
      ),
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/' + item.headerImagePath),
                fit: BoxFit.cover,
              )
            ),
            child: BackdropFilter(
              filter: ui.ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                color: Color.fromRGBO(1, 17, 38, 0.7),
              ),
            )
          ),
          ListView(
            children: [
              Container(
                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Color.fromRGBO(217, 82, 4, 1)))),
                child: Image(image: AssetImage('assets/' + item.headerImagePath))
              ),
              Padding(
                padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                child: MediaQuery(
                  data: MediaQueryData(textScaleFactor: MediaQuery.textScaleFactorOf(context)),
                  child: Text(
                    item.teaser,
                    style: TextStyle(
                      color: Color.fromRGBO(242, 235, 236, 1),
                      fontSize: 25
                    ),
                  ),
                )
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: SingleChildScrollView(
                  child: MediaQuery(
                    data: MediaQueryData(textScaleFactor: MediaQuery.textScaleFactorOf(context)),
                    child: Text(
                      item.detail,
                      textAlign: TextAlign.justify,
                      overflow: TextOverflow.visible,
                      style: TextStyle(color: Color.fromRGBO(242, 235, 236, 1))
                    ),
                  ),
                )
              ),
            ],
          )
        ],
      )
    );
  }
}