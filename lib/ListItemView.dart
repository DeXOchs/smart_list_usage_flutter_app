import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './UsableContentItem.dart';

class ListItemView extends StatefulWidget {
  ListItemView({Key key, this.listItems, this.index}) : super(key: key);
  
  final List<UsableContentItem> listItems;
  final int index;
  
  @override
  _ListItemViewState createState() => _ListItemViewState();
}

class _ListItemViewState extends State<ListItemView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(7),
        color: Colors.black.withOpacity(0.6),
      ),
      child: Stack(
        fit: StackFit.expand,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(7)),
            child: Image(
              image: AssetImage('assets/' + widget.listItems[widget.index].headerImagePath),
              fit: BoxFit.cover,
            )
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(bottom: 10, left: 15, right: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(7)),
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0x00000000),
                    Colors.black
                  ]
                )
              ),
              child: Text(
                widget.listItems[widget.index].teaser,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color.fromRGBO(242, 235, 236, 1),
                  fontSize: 25,
                ),
              )
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(7)),
            child: Container(
              decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Color.fromRGBO(217, 82, 4, 1)))),
            )
          ),
        ],
      )
    );
  }
}