import 'package:flutter/cupertino.dart';

class UsableContentItem {
  final String headerImagePath;
  final String teaser;
  final String detail;
  
  UsableContentItem({
    this.headerImagePath,
    this.teaser,
    this.detail});
  
  doPreferredAction(BuildContext context) {
    var alert = CupertinoAlertDialog(
      title: Text('Action triggered'),
      content: Text('Imagine anything ...'),
      actions: [
        CupertinoDialogAction(
          child: Text('Close Dialog'),
          isDefaultAction: true,
          onPressed: () => Navigator.pop(context),
        )
      ],
    );
  
    showCupertinoDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      }
    );
  }
}