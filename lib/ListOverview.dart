import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './UsableContentItem.dart';
import './ListItemView.dart';
import './DetailView.dart';

class ListOverview extends StatefulWidget {
  ListOverview({Key key, this.listItems, this.actionDescription}) : super(key: key);
  
  final List<UsableContentItem> listItems;
  final String actionDescription;
  
  @override
  _ListOverviewState createState() => _ListOverviewState();
}

class _ListOverviewState extends State<ListOverview> {
  
  PageController pageViewController = new PageController();
  
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      resizeToAvoidBottomInset: true,
      child: Stack(
        children: [
          Positioned.fill(
            child: Image(
              fit: BoxFit.cover,
              image: AssetImage('assets/bg.jpg'),
            ),
          ),
          PageView.builder(
            controller: pageViewController,
            itemCount: widget.listItems.length,
            itemBuilder: (BuildContext context,int index) {
              return Center(
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  height: MediaQuery.of(context).size.height * 0.75,
                  child: GestureDetector(
                    child: ListItemView(
                      listItems: widget.listItems,
                      index: index,
                    ),
                    onTap: () => chooseCurrent()
                  )
                )
              );
            }
          ),
        ],
      )
    );
  }
  
  void viewRight() => pageViewController.animateToPage(
    (pageViewController.page.round() + 1) % widget.listItems.length,
    duration: Duration(milliseconds: 500),
    curve: Curves.easeInOut
  );
  
  void viewLeft() => pageViewController.animateToPage(
    (pageViewController.page.round() - 1) % widget.listItems.length,
    duration: Duration(milliseconds: 500),
    curve: Curves.easeInOut
  );
  
  void chooseCurrent() {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => DetailView(
          item: widget.listItems[pageViewController.page.round()],
          actionDescription: widget.actionDescription,
        )
      )
    );
  }
  
  void doPreferredAction() => widget.listItems[pageViewController.page.round()].doPreferredAction(context);
}