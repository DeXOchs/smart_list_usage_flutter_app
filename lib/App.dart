import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './UsableList.dart';
import './ListOverview.dart';

class App extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: 'SLUA',
      theme: CupertinoThemeData(
        primaryColor: Color.fromRGBO(217, 82, 4, 1),
      ),
      home: ListOverview(
        listItems: new UsableList().getList(),
        actionDescription: new UsableList().getActionDescription(),
      )
    );
  }
}